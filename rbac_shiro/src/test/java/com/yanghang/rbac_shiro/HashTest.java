package com.yanghang.rbac_shiro;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.junit.Test;

/**
 * @program: shiro_code
 * @description: 转化Hash测试类
 * @author: YangHang
 * @create: 2019-06-26 21:23
 **/

public class HashTest {

    @Test
    public void test() {
        String hashName = "md5";

        String pwd = "123456";

        Object result = new SimpleHash(hashName, pwd, null, 2);

        System.out.println(result);
    }
}
