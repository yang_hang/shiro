package com.yanghang.rbac_shiro.config;

import com.yanghang.rbac_shiro.domain.PermissionEntity;
import com.yanghang.rbac_shiro.domain.RoleEntity;
import com.yanghang.rbac_shiro.domain.UserEntity;
import com.yanghang.rbac_shiro.repository.PermissionEntityRepository;
import com.yanghang.rbac_shiro.repository.RoleEntityRepository;
import com.yanghang.rbac_shiro.repository.UserEntityRepository;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: shiro_code
 * @description: 自定义Realm数据源
 * @author: YangHang
 * @create: 2019-06-25 00:20
 **/

public class CustomRealm extends AuthorizingRealm {

    @Autowired
    private UserEntityRepository userEntityRepository;

    @Autowired
    private RoleEntityRepository roleEntityRepository;

    @Autowired
    private PermissionEntityRepository permissionEntityRepository;


    /**
     * @Description: 授权时调用
     * @Param: [principalCollection]
     * @return: org.apache.shiro.authz.AuthorizationInfo
     * @Author: YangHang
     * @Date: 2019/6/25 0:25
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {

        Assert.isTrue(!principalCollection.isEmpty(), "唯一标识不能为空!!!");
        // 1.获取用户唯一表示, 这里是用户名
        String username = (String) principalCollection.getPrimaryPrincipal();
        // 2.通过用户名获得该用户下的所有角色, 再通过角色获取角色下面的权限
        List<RoleEntity> roleEntityList = roleEntityRepository.findRoleEntityByUsername(username);
        List<String> roleNameList = roleEntityList.stream().map(roleEntity -> roleEntity.getName()).distinct().collect(Collectors.toList());
        List<PermissionEntity> permissionEntityList = permissionEntityRepository.findPermissionEntityByRoleName(roleNameList);
        List<String> permissionNameList = permissionEntityList.stream().map(permissionEntity -> permissionEntity.getName()).distinct().collect(Collectors.toList());
        // 3.组装要返回的认证信息
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo() {{
            addRoles(roleNameList);
            addStringPermissions(permissionNameList);
        }};
        return authorizationInfo;
    }

    /**
     * @Description: 登录的时候调用
     * @Param: [authenticationToken]
     * @return: org.apache.shiro.authc.AuthenticationInfo
     * @Author: YangHang
     * @Date: 2019/6/25 0:26
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        // 1.获取唯一标识, 即用户名
        String username = (String) authenticationToken.getPrincipal();
        // 2.通过用户名获取当前用户对象
//        Optional<UserEntity> userEntity = userEntityRepository.findOne(Example.of(new UserEntity() {{
//            setUsername(username);
//        }}));
        UserEntity userEntity = userEntityRepository.findByUsername(username);
        Assert.notNull(userEntity, "user does not exists！！！");
        String password = userEntity.getPassword();
        // 3.返回用户认证信息
        return new SimpleAuthenticationInfo(username, password, this.getName());
    }
}
