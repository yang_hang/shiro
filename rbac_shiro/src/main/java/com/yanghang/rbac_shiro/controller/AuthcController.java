package com.yanghang.rbac_shiro.controller;

import com.yanghang.rbac_shiro.domain.JsonData;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

/**
 * @program: shiro_code
 * @description: 登录后才能访问的客户订单
 * @author: YangHang
 * @create: 2019-06-26 22:17
 **/

@RestController
@RequestMapping("auth")
public class AuthcController {

    /**
     * @Description: 查找用户的订单
     * @return: void
     * @Author: YangHang
     * @Date: 2019/6/26 22:19
     */
    @GetMapping("findUserGoods")
    public JsonData findUserGoods() {
        List<String> goodList = Arrays.asList("Redis缓存", "JVM调优", "设计模式");
        return JsonData.buildSuccess(goodList);
    }

}
