package com.yanghang.rbac_shiro.controller;

import com.yanghang.rbac_shiro.domain.JsonData;
import com.yanghang.rbac_shiro.dto.UserDto;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * @program: shiro_code
 * @description: 公共控制类， 任何人都可以访问
 * @author: YangHang
 * @create: 2019-06-24 22:18
 **/

@RestController
@RequestMapping("pub")
public class PubController {

    /**
     * @Description: 用戶登录的时候所调用的方法
     * @Param: [userDto] 用户信息DTO
     * @return: net.xdclass.rbac_shiro.domain.JsonData
     * @Author: YangHang
     * @Date: 2019/6/26 20:41
     */
    @RequestMapping("login")
    public JsonData login(@RequestBody UserDto userDto) {

        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(userDto.getUsername(), userDto.getPassword());
        try {
            // 登录成功
            Map info = new HashMap();
            info.put("msg", "登录成功！！！");
            info.put("session_id", subject.getSession().getId());
            subject.login(token);
            return JsonData.buildSuccess(info, "登录成功！！！");
        } catch (AuthenticationException e) {
            // 登录失败
            e.printStackTrace();
            return JsonData.buildError("用户名或密码不正确！！！");
        }
    }

    /** 
    * @Description: 当被需要登录才能访问的过滤器拦截之后调用此接口 
    * @Param: [] 
    * @return: com.yanghang.rbac_shiro.domain.JsonData 
    * @Author: YangHang
    * @Date: 2019/6/26 21:02
    */ 
    @RequestMapping("need_login")
    public JsonData needLogin() {
        return JsonData.buildError("用户需要登录才能访问该接口");
    }

    /** 
    * @Description: 当被需要授权才能访问的过滤器拦截之后调用此接口
    * @Param: [] 
    * @return: com.yanghang.rbac_shiro.domain.JsonData 
    * @Author: YangHang
    * @Date: 2019/6/26 21:11
    */ 
    @RequestMapping("not_permit")
    public JsonData notPermit() {
        return JsonData.buildError("用户没有访问该接口的权限");
    }


    @RequestMapping("picture")
    public void picture(HttpServletResponse response) {

        try {
            File file = new File(this.getClass().getResource("/photo/a.jpg").getPath());
            FileInputStream fis;
            fis = new FileInputStream(file);

            long size = file.length();
            byte[] temp = new byte[(int) size];
            fis.read(temp, 0, (int) size);
            fis.close();
            byte[] data = temp;
            response.setContentType("image/png");
            OutputStream out = response.getOutputStream();
            out.write(data);
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
