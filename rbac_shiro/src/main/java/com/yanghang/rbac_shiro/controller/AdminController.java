package com.yanghang.rbac_shiro.controller;

import com.yanghang.rbac_shiro.domain.JsonData;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: shiro_code
 * @description: 管理员模块
 * @author: YangHang
 * @create: 2019-06-26 22:41
 **/

@RestController
@RequestMapping("admin")
public class AdminController {

    /** 
    * @Description: 查询当前系统有多少用户
    * @Param: [] 
    * @return: com.yanghang.rbac_shiro.domain.JsonData 
    * @Author: YangHang
    * @Date: 2019/6/27 0:51
    */ 
    @RequestMapping("count")
    public JsonData findUserNum() {
        return JsonData.buildSuccess(788);
    }

}
