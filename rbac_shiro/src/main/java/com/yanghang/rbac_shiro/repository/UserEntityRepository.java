package com.yanghang.rbac_shiro.repository;

import com.yanghang.rbac_shiro.domain.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserEntityRepository extends JpaRepository<UserEntity,Integer> {
    
    /**
    * @Description: 根据用户名查找用户
    * @Param: [username]
    * @return: com.yanghang.rbac_shiro.domain.UserEntity
    * @Author: YangHang
    * @Date: 2019/6/26 21:44
    */
    public UserEntity findByUsername(String username);
}
