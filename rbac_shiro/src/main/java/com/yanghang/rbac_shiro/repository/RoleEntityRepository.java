package com.yanghang.rbac_shiro.repository;

import com.yanghang.rbac_shiro.domain.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RoleEntityRepository extends JpaRepository<RoleEntity,Integer> {

    /** 
    * @Description: 通过用户名查找该用户所拥有的的所有角色
    * @Param: [username] 
    * @return: java.util.List<com.yanghang.rbac_shiro.domain.RoleEntity> 
    * @Author: YangHang
    * @Date: 2019/6/25 0:56
    */ 
    @Query("select s from RoleEntity s inner join UserRoleEntity a on a.roleId = s.id inner join UserEntity b on b.id = a.userId where b.username = ?1")
    List<RoleEntity> findRoleEntityByUsername(String username);
}
