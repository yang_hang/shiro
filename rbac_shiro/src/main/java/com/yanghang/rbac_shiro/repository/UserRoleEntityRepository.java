package com.yanghang.rbac_shiro.repository;

import com.yanghang.rbac_shiro.domain.UserRoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRoleEntityRepository extends JpaRepository<UserRoleEntity,Integer> {
}
