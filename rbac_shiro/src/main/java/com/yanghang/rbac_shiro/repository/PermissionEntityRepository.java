package com.yanghang.rbac_shiro.repository;

import com.yanghang.rbac_shiro.domain.PermissionEntity;
import com.yanghang.rbac_shiro.domain.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface PermissionEntityRepository extends JpaRepository<PermissionEntity, Integer> {

    /** 
    * @Description: 通过角色集合名查找所有角色下面的所有权限
    * @Param: [roleNameList]
    * @return: java.util.List<com.yanghang.rbac_shiro.domain.PermissionEntity> 
    * @Author: YangHang
    * @Date: 2019/6/25 0:56
    */ 
    @Query("select s from PermissionEntity s inner join RolePermissionEntity a on a.permissionId = s.id inner join RoleEntity b on b.id = a.roleId where b.name in (?1)")
    List<PermissionEntity> findPermissionEntityByRoleName(List<String> roleNameList);
}
