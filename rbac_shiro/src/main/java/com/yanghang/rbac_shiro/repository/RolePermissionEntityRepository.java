package com.yanghang.rbac_shiro.repository;

import com.yanghang.rbac_shiro.domain.RolePermissionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RolePermissionEntityRepository extends JpaRepository<RolePermissionEntity,Integer> {
}
