package com.yanghang.rbac_shiro.service.impl;

import com.yanghang.rbac_shiro.domain.UserEntity;
import com.yanghang.rbac_shiro.dto.UserDto;
import com.yanghang.rbac_shiro.repository.UserEntityRepository;
import com.yanghang.rbac_shiro.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @program: shiro_code
 * @description: 用户服务实现类
 * @author: YangHang
 * @create: 2019-06-24 22:04
 **/

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserEntityRepository userEntityRepository;

    @Override
    public UserDto findById(Integer id) throws Exception {
        Optional<UserEntity> userEntity = userEntityRepository.findById(id);
        UserDto userDto = new UserDto();
        BeanUtils.copyProperties(userEntity.get(),userDto);
        return userDto;
    }
}
