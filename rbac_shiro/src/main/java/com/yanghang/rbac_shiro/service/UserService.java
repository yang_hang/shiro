package com.yanghang.rbac_shiro.service;

import com.yanghang.rbac_shiro.dto.UserDto;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;

/**
 * @program: shiro_code
 * @description: 用户服务类
 * @author: YangHang
 * @create: 2019-06-24 21:51
 **/

public interface UserService {

    /** 
    * @Description: 通过id寻找用户
    * @Param: [id] 
    * @return: com.yanghang.rbac_shiro.dto.UserDto 
    * @Author: YangHang
    * @Date: 2019/6/24 22:16
    */ 
    UserDto findById(Integer id) throws Exception;

}
