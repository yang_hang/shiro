package com.yanghang.rbac_shiro.dto;

import com.yanghang.rbac_shiro.domain.UserRoleEntity;

/**
 * @program: shiro_code
 * @description: 用户角色映射Dto类
 * @author: YangHang
 * @create: 2019-06-24 22:01
 **/

public class UserRoleDto extends UserRoleEntity {
}
