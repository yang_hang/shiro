package com.yanghang.rbac_shiro.dto;

import com.yanghang.rbac_shiro.domain.UserEntity;

import java.util.List;

/**
 * @program: shiro_code
 * @description: 用户Dto类
 * @author: YangHang
 * @create: 2019-06-24 22:01
 **/
public class UserDto extends UserEntity {

    private List<RoleDto> roleDtoList;

    public List<RoleDto> getRoleDtoList() {
        return roleDtoList;
    }

    public void setRoleDtoList(List<RoleDto> roleDtoList) {
        this.roleDtoList = roleDtoList;
    }
}
