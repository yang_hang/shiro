package com.yanghang.rbac_shiro.dto;

import com.yanghang.rbac_shiro.domain.RolePermissionEntity;

/**
 * @program: shiro_code
 * @description: 角色权限映射类
 * @author: YangHang
 * @create: 2019-06-24 22:00
 **/

public class RolePermissionDto extends RolePermissionEntity {
}
