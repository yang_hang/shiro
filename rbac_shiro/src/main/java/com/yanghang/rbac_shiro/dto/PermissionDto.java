package com.yanghang.rbac_shiro.dto;

import com.yanghang.rbac_shiro.domain.PermissionEntity;

/**
 * @program: shiro_code
 * @description: 权限Dto类
 * @author: YangHang
 * @create: 2019-06-24 21:58
 **/

public class PermissionDto extends PermissionEntity {
}
