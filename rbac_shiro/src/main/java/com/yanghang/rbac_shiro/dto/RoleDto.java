package com.yanghang.rbac_shiro.dto;

import com.yanghang.rbac_shiro.domain.RoleEntity;

/**
 * @program: shiro_code
 * @description: 角色Dto类
 * @author: YangHang
 * @create: 2019-06-24 21:59
 **/

public class RoleDto extends RoleEntity {
}
