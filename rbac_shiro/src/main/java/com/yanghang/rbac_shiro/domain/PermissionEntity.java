package com.yanghang.rbac_shiro.domain;

import javax.persistence.*;
import java.util.Objects;

/**
 * @program: shiro_code
 * @description:
 * @author: YangHang
 * @create: 2019-06-24 21:24
 **/

@Entity
@Table(name = "permission", schema = "shiro", catalog = "")
public class PermissionEntity {
    private int id;
    private String name;
    private String url;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PermissionEntity that = (PermissionEntity) o;
        return id == that.id &&
                Objects.equals(name, that.name) &&
                Objects.equals(url, that.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, url);
    }
}
