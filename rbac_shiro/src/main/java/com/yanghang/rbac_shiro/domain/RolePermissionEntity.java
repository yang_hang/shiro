package com.yanghang.rbac_shiro.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * @program: shiro_code
 * @description:
 * @author: YangHang
 * @create: 2019-06-24 21:24
 **/

@Entity
@Table(name = "role_permission", schema = "shiro", catalog = "")
public class RolePermissionEntity implements Serializable {
    private int id;
    private Integer roleId;
    private Integer permissionId;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "role_id")
    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    @Basic
    @Column(name = "permission_id")
    public Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RolePermissionEntity that = (RolePermissionEntity) o;
        return id == that.id &&
                Objects.equals(roleId, that.roleId) &&
                Objects.equals(permissionId, that.permissionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, roleId, permissionId);
    }
}
